import React  from 'react';
import styled from 'styled-components';

import HeroContent from './HeroContent/index.jsx';

import Transition from 'mcfinley-simple-react-transition';

class Hero extends React.Component {
  render () {

    /* Stars array */
    const stars = (
      <g className='hero__stars'>
        {
          Array(200).fill(0).map(function () {
            return {
              x: Math.random() * 2000,
              y: Math.random() * 2000 - 1000,
              r: Math.random() * 5,
              fill: 'rgba(255, 255, 255, ' + Math.random() + ')',
              style: { animationDelay: Math.random() * 10 + 's' }
            };
          }).map(function (point, index) {
            return <circle key={index} cx={point.x} cy={point.y} r={point.r} fill={point.fill} style={point.style} />;
          })
        }
      </g>
    );

    /* Comets array */
    const comets = (
      <g className='hero__comets'>
        {
          Array(0).fill(0).map(function () {
            const startX = Math.random() * 3000;
            const startY = -100;
            const len = 4000;
            return {
              x1: startX,
              y1: startY,
              x2: startX - len,
              y2: startY + len,
              style: {
                stroke: '#fff',
                strokeWidth: '1px',
                animationDelay: Math.random() * 10 + 's',
                animationDuration: (2 + Math.random() * 5) + 's'
              }
            };
          }).map(function (line) {
            return <line x1={line.x1} y1={line.y1} x2={line.x2} y2={line.y2} style={line.style} />;
          })
        }
      </g>
    );

    /* */
    return (
      <Transition
        component="div"
        className={this.props.className}
        visible={this.props.routeShow}
        afterEnter={300}
        beforeLeave={300}
        cssName="main-animation"
        >
        <svg className='hero__svg' >
          {stars}
          {comets}
        </svg>
        <div className='hero__content'>
          <HeroContent />
        </div>
      </Transition>
    );
  }
};

export default styled(Hero)`

  /* Text content animation */
  &.main-animation {
    &-after-enter {
      & { opacity: 0.0 }
      &-active { opacity: 1; transition: all 300ms linear; position: absolute }
    }
    &-before-leave {
      & { opacity: 1.0 }
      &-active { opacity: 0; transition: all 300ms linear; position: absolute }
    }
  }

  background: #2f3438;
  width: 100%;
  overflow: hidden;
  height: 100vh;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;

  .hero__svg {
    width: 2500px;
    height: 100%;
    margin: 0 auto;
    position: absolute;
  }

  .hero__stars {
    transform-origin: 50% 100px;
    animation-name: hero__stars;
    animation-duration: 200s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
  }

  .hero__stars circle {
    transform-origin: 50% 50%;
    animation-name: hero__star;
    animation-duration: 10s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
  }

  .hero__comets line {
    stroke-dasharray: 50 1200;
    animation-name: hero__comet;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
  }

  .hero__content {
    position: relative;
    z-index: 10;
  }

  @keyframes hero__stars {
    from {
      transform: rotate(0);
    }
    to {
      transform: rotate(360deg);
    }
  }

  @keyframes hero__star {
    0% {
      transform: scale(1);
    }
    98% {
      transform: scale(1);
    }
    99% {
      transform: scale(0.5);
    }
    100% {
      transform: scale(1);
    }
  }

  @keyframes hero__comet {
    0% {
      stroke-dashoffset: 0%;
    }
    100% {
      stroke-dashoffset: 100%;
    }
  }

`;
