﻿//some added strings
//tmp.X=decX;
//tmp.Y=decY;
//tmp.Z=decZ;
//
package SingleClass;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kate
 */
public class Point3D {
    private double X;
    private double Y;
    private double Z;
    private double R;
    private double angle; 
      
    private Point3D() 
    {}
   
    /**
     * Породить точку в декартовых координатах
     * @param decX Координата X
     * @param decY Координата Y
     * @param decZ Координата Z
     * @return Новая точка в декартовых координатах
     */
    public static Point3D newDecart(double decX, double decY, double decZ){
        Point3D tmp=new Point3D();
        tmp.X=decX;
        tmp.Y=decY;
        tmp.Z=decZ;
        tmp.R=Math.pow((decX*decX+decY*decY), 0.5);
        if (Math.abs(decX-decY)<0.00000001)
            tmp.angle=0;
        else
            tmp.angle=Math.acos(decX/Math.pow((decX*decX+decY*decY), 0.5));  
        return tmp;
    }
Step two
tmp.X=decX;
tmp.Y=decY;
tmp.Z=decZ;
    /**
     * Породить точку в полярных координатах
     * @param polarR Радиус-вектор
     * @param angle Полярный угол
     * @param Z Координата Z
     * @return Новая точка в полярных координатах
     */
    public static Point3D newPolar(double polarR, double angle, double Z){
        Point3D tmp=new Point3D();
        tmp.X=polarR*Math.cos(angle); 
        tmp.Y=polarR*Math.sin(angle);
        tmp.Z=Z;
        tmp.R=polarR;
        tmp.angle=angle;
        return tmp;
    }
    /**
     * Предопределение начала координат в декартовой системе
     * @return Начало координат
     */
    public static Point3D originOfCoordinates(){
        Point3D tmp=new Point3D();
        tmp.X=0; 
        tmp.Y=0;
        tmp.Z=0;
        tmp.R=0;
        tmp.angle=0;
        return tmp;
    }

Step 3
a+b

    /**
     * Определить расстояние между двумя точками
     * @param other Вторая точка
     * @return Расстояние между точками
     */
    public double distanceTo (Point3D other){
        double distance;
        distance=Math.pow((Math.pow((this.X-other.X), 2.0))+(Math.pow((this.Y-other.Y), 2.0))+(Math.pow((this.Z-other.Z), 2.0)), 0.5);
        return distance;
    }
    /**
     * Определить расстояние до начала координат
     * @return Расстояние
     */
    public double distanceToOriginOfCoordinates () {
        double distance;
        distance=Math.pow((Math.pow(this.X, 2.0))+(Math.pow(this.Y, 2.0))+(Math.pow(this.Z, 2.0)), 0.5);
        return distance;
    }
    
    /**
     * Масштабировать точку
     * @param scailingFactor Коэффициент масштабирования
     * @return Новая масштабированная точка
     */
    public Point3D scale(double scailingFactor) {
        Point3D tmp=new Point3D();
        tmp.X=this.X*scailingFactor;
        tmp.Y=this.Y*scailingFactor;
        tmp.Z=this.Z*scailingFactor;
        tmp.R=Math.pow((tmp.X*tmp.X+ tmp.Y* tmp.Y), 0.5);
        tmp.angle=Math.acos(tmp.X/Math.pow((tmp.X*tmp.X+tmp.Y*tmp.Y), 0.5));  
        return tmp;
    }

    /**
     * Сравнить с заданной точностью
     * @param other Вторая точка
     * @param precision Точность
     * @return Результат сравнения: true - точки равны, false - точки не равны
     */ 
    public boolean equalsWithPrecision(Point3D other, double precision){
        boolean isEqual;
        isEqual = precision>Math.abs(this.X-other.X) && precision>Math.abs(this.Y-other.Y)&& precision>Math.abs(this.Z-other.Z);
        return isEqual;
    }
Step five
boolean isEqual;    

    @Override
    public boolean equals(Object other){
        boolean isEqual;
        if (other == null || !(other instanceof Point3D))
            isEqual=false; 
        else {
        Point3D point = (Point3D)other; 
        isEqual = 0.00000001>Math.abs(this.X-point.X) && 0.000000001>Math.abs(this.Y-point.Y)&& 0.000000001>Math.abs(this.Z-point.Z);
        }
        return isEqual;
    }
}
