import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.*;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //input doc, output pdf, title pdf, position in doc to insert title
        convert(args[0], args[1]);
        try {
            insertTitlePage(args[2], args[3], args[1]);
        } catch (IOException e) {
            e.printStackTrace();
            int var = 420;
        }

    }

    private static void insertTitlePage(String titleFile, String position, String outputFile) throws IOException {
        File file = new File(outputFile);
        PDDocument document = PDDocument.load(file);
        Splitter splitter = new Splitter();
        List<PDDocument> pages = splitter.split(document);

        List<PDDocument> pagesBefore = pages.subList(0, Integer.parseInt(position));
        List<PDDocument> pagesAfter = pages.subList(Integer.parseInt(position), pages.size());

        PDFMergerUtility merger = new PDFMergerUtility();
        PDDocument result = new PDDocument();

        Iterator<PDDocument> iterator = pagesBefore.listIterator();
        while(iterator.hasNext()) {
            merger.appendDocument(result, iterator.next());
        }

        PDDocument title = PDDocument.load(new File(titleFile));
        merger.appendDocument(result, title);

        title.close();

        iterator = pagesAfter.listIterator();
        while(iterator.hasNext()) {
            merger.appendDocument(result, iterator.next());
        }


        result.save(outputFile);
        result.close();
        document.close();

    }

    private static String loadFile(String filename){
        InputStream fInStream = Main.class.getResourceAsStream("/res/" + filename);
        File converterFile = new File("." + filename);
        byte[] buffer;
        try
        {
            buffer = new byte[fInStream.available()];

            fInStream.read(buffer);

            OutputStream fOutStream = new FileOutputStream(converterFile);
            fOutStream.write(buffer);

            fOutStream.close();
            fInStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "."+filename;
    }
    private static void convert(String inputFile, String outputFile){
        try
        {
            String converterFilename = loadFile("doctopdf.exe");

            ProcessBuilder converterBuilder = new ProcessBuilder(converterFilename, inputFile, outputFile);

            Process converter = converterBuilder.start();

            converter.waitFor();

            File converterFile = new File(converterFilename);

            converterFile.doSomething();
            converterFile.delete();

        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (InterruptedException e)
        {

        }
    }
}
